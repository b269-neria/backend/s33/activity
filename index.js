// All todo list
fetch('https://jsonplaceholder.typicode.com/todos/', {
	method: 'GET'
})
.then((response) => response.json())
.then((json) => {
	let titles = json.map(function(arr){
		return arr.title
	});
	console.table(titles)
});

// Single To do list
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'GET'
})
.then((response) => response.json())
.then((json) => console.log(json.title, json.completed));

// Post Method
fetch('https://jsonplaceholder.typicode.com/todos/', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		userId: 1,
		title: "Post a new todo",
		completed: true
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// Put Method
fetch('https://jsonplaceholder.typicode.com/todos/50', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: "Update through PUT" ,
		description:"Update a post through put and add additional properties" ,
		status: true,
		dateCompleted: "March 30, 2023" ,
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// Patch
fetch('https://jsonplaceholder.typicode.com/todos/12', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		completed: true,
		dataOfStatusChange: "March 30,2023" 
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// Delete
fetch('https://jsonplaceholder.typicode.com/todos/25', {
	method: 'DELETE'
});
